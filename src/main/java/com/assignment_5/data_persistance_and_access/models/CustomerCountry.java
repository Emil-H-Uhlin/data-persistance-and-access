package com.assignment_5.data_persistance_and_access.models;

public record CustomerCountry(String country,
                              int numCustomers) {
}
