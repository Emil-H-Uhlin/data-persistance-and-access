package com.assignment_5.data_persistance_and_access.models;

public record CustomerGenre(String[] genres,
                            int trackCount) {
}
