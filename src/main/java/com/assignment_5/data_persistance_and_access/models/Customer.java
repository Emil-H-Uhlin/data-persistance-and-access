package com.assignment_5.data_persistance_and_access.models;

import org.springframework.lang.NonNull;

public record Customer(int customer_id, @NonNull String first_name, String last_name, String company, String address,
                       String city, String state, String country, String postal_code, String phone,
                       String fax, @NonNull String email) {
    
    public Customer(@NonNull String first_name, @NonNull String last_name, String company, String address, String city,
                    String state, String country, String postal_code, String phone,
                    String fax, @NonNull String email) {
        
        this(-1, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email);
    }
    
    /**
     * Modify first_name
     * @param newFirstName New first name
     * @return Customer with new frist_name
     */
    public Customer withFirstName(@NonNull String newFirstName) {
        return new Customer(customer_id, newFirstName, last_name, company, address, city, state, country,
                postal_code, phone, fax, email);
    }
    
    /**
     * Modify last_name
     * @param newLastName New last name
     * @return Customer with new last_name
     */
    public Customer withLastName(@NonNull String newLastName) {
        return new Customer(customer_id, first_name, newLastName, company, address, city, state, country,
                postal_code, phone, fax, email);
    }
    
    /**
     * Modify address
     * @param newAddress New address
     * @return Customer with changed address
     */
    public Customer withAddress(String newAddress) {
        return new Customer(customer_id, first_name, last_name, company, newAddress, city, state, country,
                postal_code, phone, fax, email);
    }
    
    /**
     * Modify city
     * @param newCity New city
     * @return Customer with updated city
     */
    public Customer withCity(String newCity) {
        return new Customer(customer_id, first_name, last_name, company, address, newCity, state, country,
                postal_code, phone, fax, email);
    }
    
    /**
     * Modify state
     * @param newState New state
     * @return Customer with updated state
     */
    public Customer withState(String newState) {
        return new Customer(customer_id, first_name, last_name, company, address, city, newState, country,
                postal_code, phone, fax, email);
    }
    
    /**
     * Modify country
     * @param newCountry New country
     * @return Customer with updated country
     */
    public Customer withCountry(String newCountry) {
        return new Customer(customer_id, first_name, last_name, company, address, city, state, newCountry,
                postal_code, phone, fax, email);
    }
    
    /**
     * Modify postal_code
     * @param newPostalCode New postal-code
     * @return Customer with updated postal_code
     */
    public Customer withPostalCode(String newPostalCode) {
        return new Customer(customer_id, first_name, last_name, company, address, city, state, country,
                newPostalCode, phone, fax, email);
    }
    
    /**
     * Modify phone number
     * @param newPhone New phone number
     * @return Customer with updated phone
     */
    public Customer withPhone(String newPhone) {
        return new Customer(customer_id, first_name, last_name, company, address, city, state, country,
                postal_code, newPhone, fax, email);
    }
    
    /**
     * Update customer fax
     * @param newFax New fax
     * @return Customer with updated fax
     */
    public Customer withFax(String newFax) {
        return new Customer(customer_id, first_name, last_name, company, address, city, state, country,
                postal_code, phone, newFax, email);
    }
    
    /**
     * Update customer email address
     * @param newEmail New email address
     * @return Customer with updated email address
     */
    public Customer withEmail(@NonNull String newEmail) {
        return new Customer(customer_id, first_name, last_name, company, address, city, state, country,
                postal_code, phone, fax, newEmail);
    }
}
