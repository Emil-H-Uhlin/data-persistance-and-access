package com.assignment_5.data_persistance_and_access;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataPersistanceAndAccessApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataPersistanceAndAccessApplication.class, args);
    }

}
