package com.assignment_5.data_persistance_and_access.runners;

import com.assignment_5.data_persistance_and_access.models.Customer;
import com.assignment_5.data_persistance_and_access.repositories.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class CustomerRunner implements ApplicationRunner {
    private final CustomerRepository customerRepository;

    public CustomerRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println(customerRepository.findByName("di"));

        Customer customer = customerRepository.findById(56);
    }
}
