package com.assignment_5.data_persistance_and_access.repositories;

import java.util.List;

/*
    Generic CRUD repository interface excluding delete-methods as
    delete/join not allowed by postgres
 */
public interface CrudRepository<T, ID>{
    List<T> findAll();
    T findById(ID _int);
    int insert(T object);
    int update(T object);
}
