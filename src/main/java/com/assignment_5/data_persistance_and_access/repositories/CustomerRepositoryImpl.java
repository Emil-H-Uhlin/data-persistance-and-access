package com.assignment_5.data_persistance_and_access.repositories;

import com.assignment_5.data_persistance_and_access.models.Customer;
import com.assignment_5.data_persistance_and_access.models.CustomerCountry;
import com.assignment_5.data_persistance_and_access.models.CustomerGenre;
import com.assignment_5.data_persistance_and_access.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private final String url,
            username,
            password;
    
    /* Insert */
    public CustomerRepositoryImpl(@Value("${spring.datasource.url}")String url,
                                  @Value("${spring.datasource.username}")String username,
                                  @Value("${spring.datasource.password}")String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }
    
    /**
     * Find all customers in database
     * @return A list of customer records
     */
    @Override
    public List<Customer> findAll() {
        var customers = new ArrayList<Customer>();

        String sql = "SELECT * FROM customer";

        try (var conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn.prepareStatement(sql);
            var results = statement.executeQuery();

            while (results.next()) {
                customers.add(
                        new Customer(
                                results.getInt("customer_id"),
                                results.getString("first_name"),
                                results.getString("last_name"),
                                results.getString("company"),
                                results.getString("address"),
                                results.getString("city"),
                                results.getString("state"),
                                results.getString("country"),
                                results.getString("postal_code"),
                                results.getString("phone"),
                                results.getString("fax"),
                                results.getString("email"))
                );
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }
    
    /**
     * Finds a customer given an ID
     * @param id The ID of the customer
     * @return A customer or null if not found
     */
    @Override
    public Customer findById(Integer id) {
        Customer customer = null;

        String sql = "SELECT * FROM customer WHERE customer_id = ?";

        try (var conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn.prepareStatement(sql);
            statement.setInt(1, id);

            var results = statement.executeQuery();

            while (results.next()) {
                customer = new Customer(
                                results.getInt("customer_id"),
                                results.getString("first_name"),
                                results.getString("last_name"),
                                results.getString("company"),
                                results.getString("address"),
                                results.getString("city"),
                                results.getString("state"),
                                results.getString("country"),
                                results.getString("postal_code"),
                                results.getString("phone"),
                                results.getString("fax"),
                                results.getString("email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customer;
    }
    
    /**
     * Finds customers given a name
     * @param name Customer name search
     * @return A list of customers (or empty list if no customers were found)
     */
    @Override
    public List<Customer> findByName(String name) {
        var customers = new ArrayList<Customer>();

        String sql = """
        SELECT * FROM customer
        WHERE first_name LIKE ?
        OR last_name LIKE ?
        """;

        try (var conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn.prepareStatement(sql);

            statement.setString(1, "%" + name + "%");
            statement.setString(2, "%" + name + "%");

            var results = statement.executeQuery();

            while (results.next()) {
                customers.add(
                        new Customer(
                                results.getInt("customer_id"),
                                results.getString("first_name"),
                                results.getString("last_name"),
                                results.getString("company"),
                                results.getString("address"),
                                results.getString("city"),
                                results.getString("state"),
                                results.getString("country"),
                                results.getString("postal_code"),
                                results.getString("phone"),
                                results.getString("fax"),
                                results.getString("email"))
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }
    
    /**
     * Finds an indexed range of customers
     * @param limit Max-amount of customers
     * @param offset Start-index
     * @return A list of customers (empty if no customers are on given indeces)
     */
    @Override
    public List<Customer> getPageOfCustomers(int limit, int offset) {
        var customers = new ArrayList<Customer>();

        String sql = """
                SELECT * FROM customer
                LIMIT ?
                OFFSET ?
                """;

        try (var conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            statement.setInt(2, offset);

            var results = statement.executeQuery();

            while (results.next()) {
                customers.add(
                        new Customer(
                                results.getInt("customer_id"),
                                results.getString("first_name"),
                                results.getString("last_name"),
                                results.getString("company"),
                                results.getString("address"),
                                results.getString("city"),
                                results.getString("state"),
                                results.getString("country"),
                                results.getString("postal_code"),
                                results.getString("phone"),
                                results.getString("fax"),
                                results.getString("email"))
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }
    
    /**
     * Gets the country with the most customers
     * @return A CustomerCountry record containing the amount of customers and the country
     */
    @Override
    public CustomerCountry getCountryWithMostCustomers() {
        CustomerCountry cc = null;

        String sql = """
                SELECT country, COUNT(customer) as country_count FROM customer
                GROUP BY country
                ORDER BY country_count DESC
                LIMIT 1
                """;

        try (var conn = DriverManager.getConnection(url, username, password)) {
            var preparedStatement = conn.prepareStatement(sql);

            var results = preparedStatement.executeQuery();

            while (results.next()) {
                cc = new CustomerCountry(
                        results.getString("country"),
                        results.getInt("country_count")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return cc;
    }
    
    /**
     * Gets the customer that has the highest total on existing invoices
     * @return A CustomerSpender record containing the customer item and the amount spent in total
     */
    @Override
    public CustomerSpender getMostSpendingCustomer() {
        CustomerSpender spender = null;

        String sql = """
                SELECT customer.customer_id, customer.first_name, customer.last_name, customer.company,
                customer.address, customer.city, customer.state, customer.country, customer.postal_code,
                customer.phone, customer.fax, customer.email, customer.support_rep_id, total FROM invoice
                    JOIN customer
                    ON invoice.customer_id = customer.customer_id
                ORDER BY total DESC
                LIMIT 1
                """;

        try (var conn = DriverManager.getConnection(url, username, password)) {
            var preparedStatement = conn.prepareStatement(sql);

            var results = preparedStatement.executeQuery();

            while (results.next()) {
                spender = new CustomerSpender(
                        new Customer(
                                results.getInt("customer_id"),
                                results.getString("first_name"),
                                results.getString("last_name"),
                                results.getString("company"),
                                results.getString("address"),
                                results.getString("city"),
                                results.getString("state"),
                                results.getString("country"),
                                results.getString("postal_code"),
                                results.getString("phone"),
                                results.getString("fax"),
                                results.getString("email")
                        ),
                        results.getInt("total")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return spender;
    }
    
    /**
     * Gets the favourite genre or genres of a given customer
     * @param id customer_id
     * @return A CustomerGenre record containing the genres and track count
     */
    @Override
    public CustomerGenre favouriteGenreById(int id) {
        CustomerGenre customerGenre = null;
        ArrayList<String> genres = new ArrayList<>();
        int genreCount = 0;

        String sql = """
                WITH customer_genre_counts AS (
                    SELECT genre.name, COUNT(genre.genre_id) AS genre_count
                        FROM genre
                            JOIN track
                            ON Genre.genre_id = Track.genre_id
                                JOIN invoice_line
                                ON invoice_line.track_id = track.track_id
                                    JOIN invoice
                                    ON invoice.invoice_id = invoice_line.invoice_id
                                        JOIN customer
                                        ON Invoice.customer_id = Customer.customer_id
                        WHERE customer.customer_id = ?
                        GROUP BY genre.name
                )
                (SELECT customer_genre_counts.name, customer_genre_counts.genre_count FROM customer_genre_counts
                    JOIN (SELECT MAX(genre_count) AS max_count FROM customer_genre_counts) tbl
                    ON customer_genre_counts.genre_count = tbl.max_count)
                """;

        try (var conn = DriverManager.getConnection(url, username, password)) {
            var preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);

            var results = preparedStatement.executeQuery();

            while (results.next()) {
                genres.add(results.getString("name"));
                genreCount = results.getInt("genre_count");
            }

            customerGenre = new CustomerGenre(genres.toArray(new String[0]), genreCount);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customerGenre;
    }
    
    /**
     * Inserts a customer record to database
     * @param customer Customer record
     * @return The result of the insert-statement executing (1, or 0)
     */
    @Override
    public int insert(Customer customer) {

        String sql = """
                INSERT INTO customer (first_name, last_name, company, address, 
                city, state, country, postal_code, phone, fax, email) 
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                """;

        try (var conn = DriverManager.getConnection(url, username, password)) {
            var preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customer.first_name());
            preparedStatement.setString(2, customer.last_name());
            preparedStatement.setString(3, customer.company());
            preparedStatement.setString(4, customer.address());
            preparedStatement.setString(5, customer.city());
            preparedStatement.setString(6, customer.state());
            preparedStatement.setString(7, customer.country());
            preparedStatement.setString(8, customer.postal_code());
            preparedStatement.setString(9, customer.phone());
            preparedStatement.setString(10, customer.fax());
            preparedStatement.setString(11, customer.email());

            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }
    
    /**
     * Updates a customer in DB with identical ID as local record
     * @param customer The customer record to update
     * @return The result of the update-statement (1 or 0)
     */
    @Override
    public int update(Customer customer) {
        String sql = """
                UPDATE customer
                SET first_name = ?, 
                last_name = ?,
                company = ?,
                address = ?,
                city = ?,
                state = ?,
                country = ?,
                postal_code = ?,
                phone = ?,
                fax = ?,
                email = ?
                WHERE customer_id = ?
                """;

        try (var conn = DriverManager.getConnection(url, username, password)) {
            var preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customer.first_name());
            preparedStatement.setString(2, customer.last_name());
            preparedStatement.setString(3, customer.company());
            preparedStatement.setString(4, customer.address());
            preparedStatement.setString(5, customer.city());
            preparedStatement.setString(6, customer.state());
            preparedStatement.setString(7, customer.country());
            preparedStatement.setString(8, customer.postal_code());
            preparedStatement.setString(9, customer.phone());
            preparedStatement.setString(10, customer.fax());
            preparedStatement.setString(11, customer.email());
            preparedStatement.setInt(12, customer.customer_id());

            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }
}
