package com.assignment_5.data_persistance_and_access.repositories;

import com.assignment_5.data_persistance_and_access.models.Customer;
import com.assignment_5.data_persistance_and_access.models.CustomerCountry;
import com.assignment_5.data_persistance_and_access.models.CustomerGenre;
import com.assignment_5.data_persistance_and_access.models.CustomerSpender;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    List<Customer> findByName(String name);
    List<Customer> getPageOfCustomers(int limit, int offset);

    CustomerCountry getCountryWithMostCustomers();
    CustomerSpender getMostSpendingCustomer();

    CustomerGenre favouriteGenreById(int id);
}
