## Data persistance and Access

### About
This is a project created with Java for the Java-course at Noroff. 

The folder "_sql_scripts_" contains simple queries 
for creating a database with heroes, assistants and powers using PostgreSQL and has nothing
to do with the rest of this project.

This project manipulates the sample-database Chinook which contains costumers,
invoices, genres, artists and tracks.

**ER-diagram of database**  

<img src="https://blog.xojo.com/wp-content/uploads/2016/04/ChinookDatabaseSchema1.1.png">

#### Queries
The queries implemented in this project are:
* Read all the costumers 
* Read a specific customer by id
* Read a specific customer by name
* Return a page of customers
* Insert new customer
* Update customer
* Return the country with the most customers
* Return the customer who is the highest spender
* Return the most popular genre/-s of a given customer

The implementations for these queries can be found in `/repositories/CustomerRepositoryImpl.java.`


### Contributors
This project is created by Emil Uhlin (@emil-h-uhlin) and Tilda Källström (@tildakallstrom).


### Install/Run
In IntelliJ: 
file - new - project from Version Control - paste https://gitlab.com/Emil-H-Uhlin/data-persistance-and-access.git
To try out the functionality - add queries to the run method in /CustomerRunner.java class.

Example of snippet that would update customer - FirstName and LastName - with Id 13:
```Java
@Override
public void run(ApplicationArguments args) throws Exception {
    Customer customer = customerRepository.findById(13);
    customer = customer.withFirstName("Pepsi")
                       .withLastName("Cola");
    customerRepository.update(customer);
}
```


