INSERT INTO powers (p_name, p_desc)
VALUES ('Healing', 'Restores health');

INSERT INTO powers (p_name, p_desc)
VALUES ('Waterbreathing', 'Can breath under water');

INSERT INTO powers (p_name, p_desc)
VALUES ('Avada kedavra', 'Kills Harry Potter');

INSERT INTO powers (p_name, p_desc)
VALUES ('Expelliarmus', 'Prevents Voldemort from killing Harry Potter');


INSERT INTO superheroes_powers (s_id, p_id)
VALUES (1, 2);

INSERT INTO superheroes_powers (s_id, p_id)
VALUES (3, 3);

INSERT INTO superheroes_powers (s_id, p_id)
VALUES (3, 4);

INSERT INTO superheroes_powers (s_id, p_id)
VALUES (2, 1);