CREATE TABLE superheroes_powers(
    s_id int NOT NULL REFERENCES superheroes,
    p_id int NOT NULL REFERENCES powers,
    PRIMARY KEY(s_id, p_id)
);