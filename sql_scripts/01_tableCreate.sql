CREATE TABLE superheroes (
    s_id serial NOT NULL PRIMARY KEY,
    s_name varchar(20) NOT NULL,
    s_alias varchar(20) NOT NULL,
    s_origin varchar(100) NOT NULL
);

CREATE TABLE assistants ( 
    a_id serial NOT NULL PRIMARY KEY,
    a_name varchar NOT NULL
);

CREATE TABLE powers (
    p_id serial NOT NULL PRIMARY KEY,
    p_name varchar(20) NOT NULL,
    p_desc varchar(100) NOT NULL 
);